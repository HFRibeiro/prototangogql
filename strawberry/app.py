import strawberry
from strawberry.asgi import GraphQL
from starlette.middleware.cors import CORSMiddleware
import asyncio
import uvicorn
import random
from datetime import datetime
from fastapi import FastAPI
import time


@strawberry.type
class ValueUpdate:
    value: int
    timestamp: str


@strawberry.type
class Query:
    @strawberry.field
    def value(self) -> int:
        return random.randint(1, 1000)

    @strawberry.field
    def total_time(self) -> float:
        return total_time


@strawberry.type
class Mutation:
    @strawberry.mutation
    def set_time(self, value: float) -> float:
        global wait_time
        wait_time = value
        return value

    @strawberry.mutation
    def set_expected_subs(self, count: int) -> int:
        global target_sub_count, total_time, subscription_count
        target_sub_count = count
        total_time = None  # Reset total time
        subscription_count = 0  # Reset subscription count
        return count


@strawberry.type
class Subscription:
    @strawberry.subscription
    async def value_updated(self) -> ValueUpdate:
        global subscription_count, first_sub_time, target_sub_count, total_time
        if subscription_count == 0:
            first_sub_time = time.time()
        subscription_count += 1
        print(f"Active subscriptions: {subscription_count}")

        if subscription_count == target_sub_count:
            total_time = time.time() - first_sub_time
            print(
                f"All {target_sub_count} subscriptions are active. "
                f"Time taken: {total_time:.2f} seconds"
            )

        try:
            while True:
                await asyncio.sleep(wait_time)
                yield ValueUpdate(
                    value=random.randint(1, 1000),
                    timestamp=datetime.now().isoformat(),
                )
        finally:
            subscription_count -= 1
            print(f"Active subscriptions: {subscription_count}")


wait_time = 0.1
subscription_count = 0
first_sub_time = None
target_sub_count = 0
total_time = None

schema = strawberry.Schema(
    query=Query, mutation=Mutation, subscription=Subscription
)
graphql_app = GraphQL(schema, debug=True)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/", graphql_app)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8002)
