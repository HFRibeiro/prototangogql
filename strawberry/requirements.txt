strawberry-graphql==0.235.0
uvicorn==0.30.1
starlette==0.37.2
fastapi==0.111.0