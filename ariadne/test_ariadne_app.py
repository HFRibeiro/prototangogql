import pytest
import json
import websockets
from httpx import AsyncClient
from starlette.testclient import TestClient
from unittest.mock import patch, AsyncMock
from app import app, state


@pytest.fixture
def client():
    return TestClient(app)


@pytest.fixture
async def async_client():
    async with AsyncClient(app=app, base_url="http://test") as ac:
        yield ac


def test_value_query(client):
    query = """
    query {
        value
    }
    """
    response = client.post("/", json={"query": query})
    assert response.status_code == 200
    data = response.json()
    assert "data" in data
    assert "value" in data["data"]
    assert isinstance(data["data"]["value"], int)


def test_total_time_query(client):
    query = """
    query {
        totalTime
    }
    """
    response = client.post("/", json={"query": query})
    assert response.status_code == 200
    data = response.json()
    assert "data" in data
    assert "totalTime" in data["data"]
    assert data["data"]["totalTime"] is None or isinstance(
        data["data"]["totalTime"], float
    )


def test_set_time_mutation(client):
    mutation = """
    mutation {
        setTime(value: 2.5)
    }
    """
    response = client.post("/", json={"query": mutation})
    assert response.status_code == 200
    data = response.json()
    assert "data" in data
    assert "setTime" in data["data"]
    assert data["data"]["setTime"] == 2.5
    assert state["waitTime"] == 2.5


def test_set_expected_subs_mutation(client):
    mutation = """
    mutation {
        setExpectedSubs(count: 3)
    }
    """
    response = client.post("/", json={"query": mutation})
    assert response.status_code == 200
    data = response.json()
    assert "data" in data
    assert "setExpectedSubs" in data["data"]
    assert data["data"]["setExpectedSubs"] == 3
    assert state["targetSubCount"] == 3
    assert state["subscriptionCount"] == 0
    assert state["totalTime"] is None


@pytest.mark.asyncio
async def test_value_updated_subscription():
    subscription = """
    subscription {
        valueUpdated {
            value
            timestamp
        }
    }
    """
    ws_url = "ws://localhost:8001/"

    mock_ws = AsyncMock()
    mock_ws.__aenter__.return_value = mock_ws
    mock_ws.recv.return_value = json.dumps(
        {
            "type": "data",
            "payload": {
                "data": {
                    "valueUpdated": {
                        "value": 123,
                        "timestamp": "2023-06-01T12:00:00Z",
                    }
                }
            },
        }
    )

    with patch("websockets.connect", return_value=mock_ws):
        async with websockets.connect(
            ws_url, subprotocols=["graphql-ws"]
        ) as ws:
            await ws.send(
                json.dumps({"type": "connection_init", "payload": {}})
            )
            await ws.recv()
            await ws.send(
                json.dumps(
                    {
                        "id": "1",
                        "type": "start",
                        "payload": {"query": subscription},
                    }
                )
            )

            response = await ws.recv()
            response_data = json.loads(response)

            assert response_data["type"] == "data"
            data = response_data["payload"]["data"]
            assert "valueUpdated" in data
            assert "value" in data["valueUpdated"]
            assert "timestamp" in data["valueUpdated"]
            assert isinstance(data["valueUpdated"]["value"], int)
            assert isinstance(data["valueUpdated"]["timestamp"], str)
