from ariadne import (
    QueryType,
    MutationType,
    SubscriptionType,
    make_executable_schema,
    gql,
)
from ariadne.asgi import GraphQL
from starlette.middleware.cors import CORSMiddleware
import asyncio
import uvicorn
import random
from datetime import datetime
from fastapi import FastAPI
import time

type_defs = gql(
    """
    type Query {
        value: Int!
        totalTime: Float
    }
    type Mutation {
        setTime(value: Float!): Float!
        setExpectedSubs(count: Int!): Int!
    }
    type ValueUpdate {
        value: Int!
        timestamp: String!
    }
    type Subscription {
        valueUpdated: ValueUpdate!
    }
"""
)

query = QueryType()
mutation = MutationType()
subscription = SubscriptionType()

state = {
    "waitTime": 0.1,
    "subscriptionCount": 0,
    "firstSubTime": None,
    "targetSubCount": 0,
    "totalTime": None,
}


@query.field("value")
def resolve_get_data(_, info):
    return random.randint(1, 1000)


@query.field("totalTime")
def resolve_total_time(_, info):
    return state["totalTime"]


@mutation.field("setTime")
def resolve_set_data(_, info, value):
    state["waitTime"] = value
    return value


@mutation.field("setExpectedSubs")
def resolve_set_expected_subs(_, info, count):
    state["targetSubCount"] = count
    state["totalTime"] = None  # Reset total time
    state["subscriptionCount"] = 0  # Reset subscription count
    return count


@subscription.source("valueUpdated")
async def value_updated_generator(_, info):
    if state["subscriptionCount"] == 0:
        state["firstSubTime"] = time.time()
    state["subscriptionCount"] += 1
    print(f"Active subscriptions: {state['subscriptionCount']}")

    if state["subscriptionCount"] == state["targetSubCount"]:
        state["totalTime"] = time.time() - state["firstSubTime"]
        print(
            f"All {state['targetSubCount']} subscriptions are active. "
            f"Time taken: {state['totalTime']:.2f} seconds"
        )

    try:
        while True:
            await asyncio.sleep(state["waitTime"])
            yield {
                "value": random.randint(1, 1000),
                "timestamp": datetime.utcnow().isoformat(),
            }
    finally:
        state["subscriptionCount"] -= 1
        print(f"Active subscriptions: {state['subscriptionCount']}")


@subscription.field("valueUpdated")
def resolve_value_updated(value, info):
    return value


schema = make_executable_schema(type_defs, query, mutation, subscription)
graphql_app = GraphQL(schema, debug=True)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/", graphql_app)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)
