// src/apolloClient.js
import { ApolloClient, InMemoryCache, split, HttpLink } from '@apollo/client';
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { SubscriptionClient } from 'subscriptions-transport-ws';

const createApolloClient = (httpUri, wsUri, shouldReconnect = true) => {
  const httpLink = new HttpLink({
    uri: httpUri,
  });

  const wsClient = new SubscriptionClient(wsUri, {
    reconnect: shouldReconnect,
  });

  const wsLink = new WebSocketLink(wsClient);

  const link = split(
    ({ query }) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      );
    },
    wsLink,
    httpLink
  );

  const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
  });

  // Attach the wsClient to the ApolloClient instance for access later
  client.wsClient = wsClient;

  return client;
};

export default createApolloClient;
