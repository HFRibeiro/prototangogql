// src/performanceLink.js
import { ApolloLink } from '@apollo/client';
import { Observable } from '@apollo/client/utilities';

const performanceLink = new ApolloLink((operation, forward) => {
    const startTime = new Date().getTime();
    let lastTime = startTime;
    return new Observable(observer => {
        const handle = forward(operation).subscribe({
            next: (result) => {
                const elapsedTime = new Date().getTime() - lastTime;
                const logMessage = `Operation: ${operation.operationName || 'Unnamed Operation'} took ${elapsedTime} ms`;
                console.log(logMessage);
                lastTime = new Date().getTime();
                const event = new CustomEvent('logEvent', { detail: { message: logMessage, time: elapsedTime } });
                window.dispatchEvent(event);
                observer.next(result);
            },
            error: (error) => observer.error(error),
            complete: () => observer.complete(),
        });

        return () => {
            handle.unsubscribe();
        };
    });
});

export default performanceLink;
