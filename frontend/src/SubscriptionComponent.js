// src/SubscriptionComponent.js
import React, { useState, useEffect, useImperativeHandle, forwardRef } from 'react';
import { gql, ApolloProvider } from '@apollo/client';

const VALUE_UPDATED_SUBSCRIPTION = gql`
  subscription {
    valueUpdated {
      value
      timestamp
    }
  }
`;

const SubscriptionComponent = forwardRef(({ client }, ref) => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [subscription, setSubscription] = useState(null);

    useImperativeHandle(ref, () => ({
        stop() {
            subscription?.unsubscribe();
        }
    }));

    useEffect(() => {
        setLoading(true);
        setError(null);
        const observable = client.subscribe({ query: VALUE_UPDATED_SUBSCRIPTION });
        const sub = observable.subscribe({
            next: ({ data }) => {
                setData(data);
                setLoading(false);
            },
            error: (err) => {
                setError(err);
                setLoading(false);
            },
        });
        setSubscription(sub);

        return () => {
            sub.unsubscribe();
        };
    }, [client]);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error: {error.message}</p>;

    return (
        <ApolloProvider client={client}>
            <div style={{
                width: "100px",
                height: "50px",
                borderRadius: "10px",
                backgroundColor: "green",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginLeft: "10px",
                marginTop: "10px",
                textAlign: "center"
            }}>
                <div style={{ lineHeight: "3em" }}>
                    {data?.valueUpdated?.value ?? 'No Data'}
                </div>
            </div>
        </ApolloProvider>
    );
});

export default SubscriptionComponent;
