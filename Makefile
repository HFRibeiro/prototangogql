# Makefile
.PHONY: test lint run

run:
	docker-compose down && docker-compose build && docker-compose up --remove-orphans

# Target to run unit tests in all subdirectories
test:
	@echo "Running unit tests in strawberry..."
	@$(MAKE) -C strawberry test
	@echo "Running unit tests in ariadne..."
	@$(MAKE) -C ariadne test

# Target to run lint tests
lint:
	@echo "Installing flake8..."
	@pip install flake8
	@echo "Linting code..."
	@flake8 --exclude=frontend/node_modules .

# Target to auto-fix linting issues
auto-lint:
	@echo "Installing black..."
	@pip install black
	@echo "Auto-fixing linting issues..."
	@black . --line-length 79
